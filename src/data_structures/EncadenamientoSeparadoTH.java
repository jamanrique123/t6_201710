package data_structures;

import java.util.Arrays;
import java.util.Iterator;

import api.IEncadenamientoSeparadoTH;

public class EncadenamientoSeparadoTH<K extends Comparable<K>, V> implements IEncadenamientoSeparadoTH<K, V>{

	private int tamanio;

	private ListaLlaveValorSecuencial<K, V>[] lista;

	private ListaEncadenada<K> listaDeLlaves;

	public EncadenamientoSeparadoTH (int m)
	{
		this.tamanio = 0;
		
		this.listaDeLlaves = new ListaEncadenada<K>();

		this.lista = new ListaLlaveValorSecuencial[m];

		for (int i = 0; i < lista.length; i++)
		{
			this.lista[i] = new ListaLlaveValorSecuencial<K, V>();
		}
	}

	@Override
	public int darTamanio() {
		return tamanio;
	}

	public double darFactorDeCarga()
	{
		return (tamanio/lista.length);
	}
	
	@Override
	public boolean estaVacia() {
		return tamanio == 0;
	}

	@Override
	public boolean tieneLlave(K llave) {

		int codigo = hash(llave);

		return lista[codigo].tieneLlave(llave);
	}

	@Override
	public V darValor(K llave) {

		int codigo = hash(llave);
		
		return lista[codigo].darValor(llave);
	}

	private int hash(K llave)
	{
		return (llave.hashCode() & 0x7fffffff) % lista.length;	
	}

	@Override
	public void insertar(K llave, V valor) {
		// TODO Auto-generated method stub

		boolean tieneLlave = tieneLlave(llave);
		//Si no contiene la llave y su valor es null, no hago nada, porque
		//No es bueno tener llaves con valor null

		if (! (!tieneLlave && valor == null) )
		{
			int codigo = hash(llave);
		
			//Si no tiene esa llave y el valor es diferente de null, voy a agregar esa llave
			if (!tieneLlave && valor != null)
			{
				lista[codigo].insertar(llave, valor);
				listaDeLlaves.agregarAlInicio(llave);
				tamanio++;
			}
			//Si contiene la llave, pero su valor es null, es que voy a hacer una eliminacion
			else if (tieneLlave && valor == null)
			{
				lista[codigo].insertar(llave, valor);				
				listaDeLlaves.eliminarNodo(llave);
				tamanio--;
			}
			//Si contiene la llave y su valor es null, solamente voy a modificar el nodo, entonces su tama�o no cambia
			else if (tieneLlave && valor != null)
			{
				lista[codigo].insertar(llave, valor);
			}
			//Si no contiene la llave y su valor es null, no hago nada, porque
			//No es bueno tener llaves con valor null
			
			rehashTable();
		}
	}

	private void insertarSinReHash(K llave, V valor) {

		boolean tieneLlave = tieneLlave(llave);
		//Si no contiene la llave y su valor es null, no hago nada, porque
		//No es bueno tener llaves con valor null

		if (! (!tieneLlave && valor == null) )
		{
			int codigo = hash(llave);

			//Si no tiene esa llave y el valor es diferente de null, voy a agregar esa llave
			if (!tieneLlave && valor != null)
			{
				lista[codigo].insertar(llave, valor);
				listaDeLlaves.agregarAlInicio(llave);
				tamanio++;
			}
			//Si contiene la llave, pero su valor es null, es que voy a hacer una eliminacion
			else if (tieneLlave && valor == null)
			{
				lista[codigo].insertar(llave, valor);
				listaDeLlaves.eliminarNodo(llave);
				tamanio--;
			}
			//Si contiene la llave y su valor es null, solamente voy a modificar el nodo, entonces su tama�o no cambia
			else if (tieneLlave && valor != null)
				lista[codigo].insertar(llave, valor);
			//Si no contiene la llave y su valor es null, no hago nada, porque
			//No es bueno tener llaves con valor null		
		}
	}
	
	public void rehashTable ()
	{
		
		double factorDeCarga = tamanio/lista.length;
			if (factorDeCarga >= 8)
			{
				//M�s grande
				resize(2*lista.length);
				
			}
			else if (factorDeCarga <= 2 && lista.length > 2)
			{
				//Miti miti
				resize(lista.length/2);
			}
	}

	private void resize(int nuevoNumeroListasColisiones) {

		if(nuevoNumeroListasColisiones != 0)
		{
			Iterator <K> iterLlaves = llaves();
			EncadenamientoSeparadoTH<K, V> nuevaTH = new EncadenamientoSeparadoTH<K, V>(nuevoNumeroListasColisiones);

			while (iterLlaves.hasNext())
			{
				K llaveActual = iterLlaves.next();
				V valorActual = darValor(llaveActual);

				nuevaTH.insertarSinReHash(llaveActual, valorActual);
			}

			this.lista = nuevaTH.lista;	
		}
	}

	@Override
	public Iterator<K> llaves() {

		ListaEncadenada<K> listaDeLlavesTemp =  new ListaEncadenada<K>();

		for (ListaLlaveValorSecuencial<K,V> listasColisiones : lista)
		{
			Iterator<K> listaLocal = listasColisiones.llaves();

			while (listaLocal.hasNext())
			{
				listaDeLlavesTemp.agregarElementoFinal(listaLocal.next());
			}
		}

		final ListaEncadenada<K> listaDeLlaves = listaDeLlavesTemp;

		Iterator<K> iterLista = new Iterator<K> () {

			Iterator<K> iterLlaves = listaDeLlaves.iterator();

			@Override
			public boolean hasNext() {
				return iterLlaves.hasNext();
			}

			@Override
			public K next() {
				return iterLlaves.next();
			}
		};

		return iterLista;
	}

	@Override
	public int[] darLongitudListas() {

		int[] longitudListas = new int[lista.length];

		for (int i = 0; i< lista.length; i++)
		{
			longitudListas[i] = lista[i].darTamanio();
		}

		return longitudListas;
	}

}
