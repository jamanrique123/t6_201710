package data_structures;

public interface ILista<T> extends Iterable<T>{
	
	/**
	 * A�ade el elemento elem al final de la lista
	 * @param elem
	 */
	public void agregarElementoFinal(T elem);
	
	/**
	 * Retorna el elemento en la posici�n pos
	 * @param pos posici�n a buscar en la lista
	 * @return elemento en la posici�n pos
	 */
	public T darElemento(int pos);
	
	/**
	 * Devuelve el tama�o de la lista
	 * @return n�mero de elementos en la lista
	 */
	public int darNumeroElementos();
	
	/**
	 * Devuelve el elemento en la posici�n actual de referencia de la lista
	 * @return elemento - es el elemento guardado en la posci�n actual de referencia de la lista
	 */
	public T darElementoPosicionActual();
	
	/**
	 * Avanza la posici�n actual de referencia en una posici�n
	 * @return true si pudo avanzar, false si es la �ltima posici�n de la lista
	 */
	public boolean avanzarSiguientePosicion();
	
	/**
	 * Retrocede la posici�n actual de referencia en una posici�n 
	 * @return true si pudo retroceder, false de lo contrario.
	 */
	public boolean retrocederPosicionAnterior();
	
	
	/**
	 * Cambia el elemento en la posici�n de entrada por parametro por el elemento por parametro
	 * @param pos posicion en la que se quiere cambiar el elemento
	 * @param elem elemento a intercambiar en la poscicion.
	 */
	
	public void cambiarElementoEn(int pos, T elem);
	
	/**
	 * Parte la lista en dos, por su mitad.
	 * Se modifica la lista inicial para contener solo su primera mitad.
	 * @return la segunda mitad de la lista.
	 */
	public ILista<T> partirPorLaMitad();

}
