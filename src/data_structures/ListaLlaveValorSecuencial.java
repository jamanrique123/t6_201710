package data_structures;

import java.util.Iterator;

import api.IListaLlaveValorSecuencial;

public class ListaLlaveValorSecuencial <K,V> implements IListaLlaveValorSecuencial<K, V>{

	private ListaEncadenada<NodoLlaveValor<K, V>> nodos;

	private ListaEncadenada<K> listaLlaves;

	private int tamanio;

	@Override
	public int darTamanio() {
		return tamanio;
	}

	@Override
	public boolean estaVacia() {
		return tamanio == 0;
	}

	public ListaLlaveValorSecuencial()
	{
		nodos =  new ListaEncadenada<NodoLlaveValor<K, V>>();
		listaLlaves = new ListaEncadenada<K>();
		tamanio = 0;
	}

	@Override
	public boolean tieneLlave(K llave) {

		boolean tiene = false;

		Iterator<NodoLlaveValor<K, V>> iterNodos = nodos.iterator();

		while (iterNodos.hasNext() && !tiene)
		{
			tiene = iterNodos.next().getLlave().equals(llave);
		}

		return tiene;
	}

	@Override
	public V darValor(K llave) {

		V buscado = null;

		Iterator<NodoLlaveValor<K, V>> iterNodos = nodos.iterator();

		while (iterNodos.hasNext() && buscado == null)
		{
			NodoLlaveValor<K, V> actual = iterNodos.next();
			buscado = actual.consultar(llave);
		}

		return buscado;
	}

	@Override
	public void insertar(K llave, V valor) {

		boolean tieneLlave = tieneLlave(llave); 

		if (! (!tieneLlave && valor == null))
		{
			if (!tieneLlave && valor != null)
			{
				NodoLlaveValor<K, V> nuevo = new NodoLlaveValor<K, V>(llave, valor);
				nodos.agregarAlInicio(nuevo);
				listaLlaves.agregarAlInicio(llave);
				tamanio++;
			}

			if (tieneLlave && valor != null)
			{
				Iterator<NodoLlaveValor<K, V>> iterNodos = nodos.iterator();

				while (iterNodos.hasNext())
				{
					NodoLlaveValor<K, V> actual = iterNodos.next();

					if (actual.getLlave().equals(llave))
					{
						actual.cambiarNodo(llave, valor);
					}
				}
			}
			
			else if (tieneLlave && valor == null)
			{
				eliminarNodo(llave);
				listaLlaves.eliminarNodo(llave);
				tamanio--;
			}
		}
	}

	@Override
	public Iterator<K> llaves() {
		return listaLlaves.iterator();
	}

	public String toString()
	{
		String respuesta = "";

		Iterator<K> llaves = llaves();

		while (llaves.hasNext())
		{
			respuesta += llaves.next();
		}

		return respuesta;
	}

	private void eliminarNodo (K pLlave)
	{
		NodoEncadenado<NodoLlaveValor<K, V>> primero = nodos.darPrimero();
		
		if (primero != null && primero.darElemento().getLlave().equals(pLlave))
			nodos.eliminarPrimerElemento();
		else if (primero != null)
		{
			NodoEncadenado<NodoLlaveValor<K, V>> actual = primero;
			boolean elimine = false;
			
			while (!elimine && actual.darSiguiente() != null)
			{
				//Si el siguiente de actual es el que quiero eliminar
				if (actual.darSiguiente().darElemento().getLlave().equals(pLlave))
				{
					actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
					elimine = true;					
				}
				//Le cambio el siguiente al actual, por su siguiente siguiente
				actual = actual.darSiguiente();
			}
		}
	}
	
}
