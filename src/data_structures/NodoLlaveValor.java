package data_structures;

public class NodoLlaveValor <K,V>{

	private K llave;
	
	private V valor;
	
	public NodoLlaveValor(K pLlave, V pValor)
	{
		llave = pLlave;
		valor = pValor;
	}

	public K getLlave() {
		return llave;
	}

	public void cambiarNodo(K nuevaLlave, V nuevoValor)
	{
		llave = nuevaLlave;
		valor = nuevoValor;
	}
	
	public V consultar (K pLlave)
	{
		if (llave.equals(pLlave))
			return valor;
		return null;
	}
	
	public String toString()
	{
		return "{" + llave + ", " +valor + "}";
	}
	
	public boolean equals(NodoLlaveValor<K, V> otro)
	{
		return otro.llave.equals(this.llave) && otro.valor.equals(this.valor);
	}
}
