package logic;

import java.util.Arrays;

public class GenerarDatos {

	private static final int RANGO_INT = 2000;
	private static final double NUMERO_LETRAS = 26;
	
	public String[] generarCadenas(int N, int k)
	{
		String[] respuesta = new String[N];
		for(int i = 0; i < N; i ++)
		{
			int numeroDeAes = (int) (Math.random()*k);
			respuesta[i] = crearStringDeAes(numeroDeAes);
		}

		return respuesta;
	}

	private String crearStringDeAes(int numeroDeAes)
	{
		String respuesta = "";
		
		int i = -1;
		
		while (i++ < numeroDeAes)
			respuesta += generarLetra();
		return respuesta;
	}

	private char generarLetra() {
		int minuscula = 32;
		char ascii = (char) (((int) (Math.random()*NUMERO_LETRAS)) + 65);
		
		if (Math.random()<0.5)
			ascii += minuscula;
		
		return ascii;
	}

	public int[] generarNumeros (int n)
	{
		int[] respuesta = new int[n];

		int contadorNumeros = 0;


		while (contadorNumeros < n)
		{
			respuesta[contadorNumeros] = generarNumero();
			contadorNumeros++;
		}


		return respuesta;
	}

	private int generarNumero() {
		// TODO Auto-generated method stub
		int respuesta = 0;
		
		respuesta = (int)  (Math.random()*(RANGO_INT));
		
		return respuesta;
	}


	public static void main(String[] args) {
		GenerarDatos generador = new GenerarDatos();

		String[] cadenasRandom = generador.generarCadenas(10, 3);
		int[] NumerosRandom = generador.generarNumeros(10);

		System.out.println(Arrays.toString(cadenasRandom));
		System.out.println(Arrays.toString(NumerosRandom));
	}
}
