package logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import VO.VODataPelicula;
import VO.VOInfoPelicula;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import data_structures.EncadenamientoSeparadoTH;

public class ManejadorPeliculas 
{
	final private static String RUTA_JSON = "./data/links_json.json";
	
	private EncadenamientoSeparadoTH<Integer, VOInfoPelicula> peliculas;
	
	public void cargar()
	{
		try(FileReader fr = new FileReader(RUTA_JSON))
		{
			Gson gson = new GsonBuilder()
			.registerTypeAdapter(int.class, integerAdapterFromString)
			.registerTypeAdapter(double.class, doubleAdapterFromString)
			.create();
			
			VODataPelicula[] peliculas = gson.fromJson(fr, VODataPelicula[].class);
			
			this.peliculas = new EncadenamientoSeparadoTH<>(1);
			for(VODataPelicula pelicula:peliculas)
			{
				VOInfoPelicula peliculaActual = pelicula.getPelicula();
				this.peliculas.insertar(Integer.parseInt(pelicula.getId()), peliculaActual);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public VOInfoPelicula consultarPelicula(int id)
	{
		return peliculas.darValor(id);
	}
	
	private static final TypeAdapter<Integer> integerAdapterFromString = new TypeAdapter<Integer>() {
		  @Override 
		  public void write(JsonWriter out, Integer value) throws IOException {
		    if (value == null) {
		      out.nullValue();
		    } else {
		      out.value(value);
		    }
		  }
		  @Override 
		  public Integer read(JsonReader in) throws IOException {
		    JsonToken peek = in.peek();
		    switch (peek) {
		    case NULL:
		    	return -1;
		    case NUMBER:
		    	return in.nextInt();
		    case STRING:
		    	String s = in.nextString();	
		    	s = s.substring(0, 4);  
		      return Integer.parseInt(s);
		    default:
		      throw new IllegalStateException("Expected NUMBER but was " + peek);
		    }
		  }
		};
		
		private static final TypeAdapter<Double> doubleAdapterFromString = new TypeAdapter<Double>() {
			  @Override 
			  public void write(JsonWriter out, Double value) throws IOException {
			    if (value == null) {
			      out.nullValue();
			    } else {
			      out.value(value);
			    }
			  }
			  @Override 
			  public Double read(JsonReader in) throws IOException {
			    JsonToken peek = in.peek();
			    switch (peek) {
			    case NULL:
			    	return -1.0;
			    case NUMBER:
			    	return in.nextDouble();
			    case STRING:
			    	String s = in.nextString();	 
			    	if(s.equals("N/A"))
			    	{
			    		return 0.0;
			    	}
			      return Double.parseDouble(s);
			    default:
			      throw new IllegalStateException("Expected NUMBER but was " + peek);
			    }
			  }
			};
			
			public static void main(String[] args)
			{
				ManejadorPeliculas manejador = new ManejadorPeliculas();
				long tiempoDeInicio = System.currentTimeMillis();
				manejador.cargar();
				long tiempoDeEjecucion = System.currentTimeMillis() - tiempoDeInicio;

				long tiempoDeInicio2 = System.currentTimeMillis();
				System.out.println(manejador.consultarPelicula(62956).getTitulo());
				long tiempoDeEjecucion2 = System.currentTimeMillis() - tiempoDeInicio2;
				int contador = 0;
				int contadorListasVacias = 0;
				int contadorListasMayorNumElementos = 0;
				int mayor = Integer.MIN_VALUE;
				int menor = Integer.MAX_VALUE;
				double promedioCarga = 0.0;
				for(int lista:manejador.peliculas.darLongitudListas())
				{
					contador ++;
					if(lista == 11) contadorListasMayorNumElementos ++;
					if(lista == 0) contadorListasVacias ++;
					if(lista > mayor) mayor = lista;
					if(lista < menor) menor = lista;
					promedioCarga += lista;
					System.out.println("Tama�o lista " + contador + ":" + lista);					
				}
				promedioCarga = promedioCarga/contador;
				System.out.println("Menor n�mero de colisiones: " + menor);
				System.out.println("Mayor n�mero de colisiones: " + mayor);
				System.out.println("El promedio de colisiones es: " + promedioCarga);
				System.out.println("N�mero listas: " + manejador.peliculas.darLongitudListas().length);
				System.out.println("N�mero elementos: " + manejador.peliculas.darTamanio());
				System.out.println("N�mero de listas vacias: " + contadorListasVacias);
				System.out.println("N�mero de listas mayor n�mero elementos: " + contadorListasMayorNumElementos);
				System.out.println("El tiempo de carga fue: " +tiempoDeEjecucion);
				System.out.println("El tiempo de busqueda fue: " + tiempoDeEjecucion2);
			}
			
}
