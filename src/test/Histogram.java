package test;

import java.util.Arrays;

import edu.princeton.cs.introcs.*;

public class Histogram {
	private double[] freq;   // freq[i] = # occurences of value i
	private double max;            // max frequency of any value
	private int maxIndex;
	private int escalaTerminaEn;
	private int escalaIniciaDesde;

	// Create a new histogram. 
	public Histogram(int n) {
		freq = new double[n];
		escalaTerminaEn = 0;
		escalaIniciaDesde = 0;
	}

	// Add one occurrence of the value i. 
	public void addDataPoint(int i) {
		freq[i]++; 
		if (freq[i] > max)
		{
			max = freq[i];
			maxIndex = i;
		}
	} 

	private void recortarCeros ()
	{
		int indicePrimerNoCero = -1;
		int indiceUltimoNoCero = -1;
		
		for (int i = 0; i < freq.length && indicePrimerNoCero == -1; i++)
		{
			if (freq[i] != 0)
				indicePrimerNoCero = i;
		}
		
		for (int i = freq.length-1; i >= 0 && indiceUltimoNoCero == -1; i--)
		{
			if (freq[i] != 0)
				indiceUltimoNoCero = i;
		}
		
		int tamanioNuevo = freq.length-(indicePrimerNoCero + ( (freq.length-1) - indiceUltimoNoCero));
		double[] auxiliar = new double[tamanioNuevo];
		

		int contadorAuxiliar = 0;
		for (int i = indicePrimerNoCero; i <= indiceUltimoNoCero; i++)
		{
			auxiliar[contadorAuxiliar++] = freq[i];
		}
		
		escalaIniciaDesde = indicePrimerNoCero;
		
		escalaTerminaEn = indiceUltimoNoCero;
		
		freq = auxiliar;
	}
	
	// draw (and scale) the histogram.
	public void draw() {
		recortarCeros();
		System.out.println("Cota inferior (N�mero m�nimo de tama�o de colisiones): " + escalaIniciaDesde);
		System.out.println("Cota superior (N�mero m�ximo de colisiones): " + escalaTerminaEn);
		System.out.println("Tama�o de mayor frecuencia: " + maxIndex + " con " + max + " repeticiones");
		System.out.println(Arrays.toString(freq));
		StdDraw.setYscale(-1, max + 1);  // to leave a little border
		StdStats.plotBars(freq);
	}

	// See Program 2.2.6.
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);       // number of coins
		int trials = Integer.parseInt(args[1]);  // number of trials

		// create the histogram
		Histogram histogram = new Histogram(n+1);
		for (int t = 0; t < trials; t++) {

			//
			int aux = 0; //Bernoulli.binomial(n); Esto es una prueba aparte de la clase importada
			System.out.println("agregu�: " + aux);
			histogram.addDataPoint(aux);
		}

		//Voy a tener 3 histrograms.
		//Cada K (10, 20, ... , 100) va a ser una rayita y el tama�o de la rayita
		//Va a ser el tama�o de la tabla de hash que qued� con ese K

		// display using standard draw
		StdDraw.setCanvasSize(500, 100);
		histogram.draw();
	} 
} 