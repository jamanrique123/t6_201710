package test;

import static org.junit.Assert.*;

import java.util.Iterator;

import logic.GenerarDatos;

import org.junit.Test;

import data_structures.EncadenamientoSeparadoTH;
import edu.princeton.cs.introcs.StdDraw;
import test.Histogram;

public class PruebaEncadenamientoSeparado
{

	private static final int INTERVALO_K = 10;

	private static final int MUESTREOS = 60;

	private static final long TIEMPO_DE_VISUALIZACION = 2000;

	private EncadenamientoSeparadoTH<String, Integer> tablaHash;

	private GenerarDatos generador;

	private int[] datosInt;

	private String[] datosString;

	private void inicializarTablaHash(int m)
	{
		tablaHash = new EncadenamientoSeparadoTH<String, Integer>(m);
	}
	private void escenario10Elementos()
	{
		tablaHash.insertar("E", 0);
		tablaHash.insertar("S", 1);
		tablaHash.insertar("T", 2);
		tablaHash.insertar("R", 3);
		tablaHash.insertar("U", 4);
		tablaHash.insertar("C", 5);
		tablaHash.insertar("T", 6);
		tablaHash.insertar("U", 7);
		tablaHash.insertar("R", 8);
		tablaHash.insertar("A", 9);
	}

	private void escenario16Elementos()
	{
		escenario10Elementos();
		tablaHash.insertar("D", 10);
		tablaHash.insertar("E", 11);
		tablaHash.insertar("D", 12);
		tablaHash.insertar("A", 13);
		tablaHash.insertar("T", 14);
		tablaHash.insertar("O", 15);
		tablaHash.insertar("S", 16);
	}

	private void escenarioAleatorio10000Elementos()
	{
		generador =  new GenerarDatos();
		datosInt = generador.generarNumeros(10000);
		datosString = generador.generarCadenas(10000, 4);	
		for(int i = 0; i < 10000; i ++)
		{
			tablaHash.insertar(datosString[i], datosInt[i]);
		}

		tablaHash.rehashTable();

		//System.out.println(Arrays.toString(datosString));
	}
	private void vaciarTablaHash()
	{
		tablaHash.insertar("E", null);
		tablaHash.insertar("S", null);
		tablaHash.insertar("T", null);
		tablaHash.insertar("R", null);
		tablaHash.insertar("U", null);
		tablaHash.insertar("C", null);
		tablaHash.insertar("T", null);
		tablaHash.insertar("U", null);
		tablaHash.insertar("R", null);
		tablaHash.insertar("A", null);
		
		tablaHash.insertar("D", null);
		tablaHash.insertar("E", null);
		
		tablaHash.insertar("D", null);
		tablaHash.insertar("A", null);
		tablaHash.insertar("T", null);
		tablaHash.insertar("O", null);
		tablaHash.insertar("S", null);
	}
	@Test
	public void test() 
	{
		//Pruebas sobre escenario 10 Elementos
		inicializarTablaHash(10/4);

		escenario10Elementos();

		//Comprobar factor de carga
		assertTrue(tablaHash.darFactorDeCarga() <= 8 && tablaHash.darFactorDeCarga() >= 2);
		
		System.out.println("Punto de control 1");
		//darValor 10 elementos iniciales
		assertEquals(new Integer(0), tablaHash.darValor("E") );
		assertEquals(new Integer(1), tablaHash.darValor("S") );
		assertEquals(new Integer(6), tablaHash.darValor("T") );
		assertEquals(new Integer(8), tablaHash.darValor("R") );
		assertEquals(new Integer(7), tablaHash.darValor("U") );
		assertEquals(new Integer(5), tablaHash.darValor("C") );
		assertEquals(new Integer(9), tablaHash.darValor("A") );
		assertEquals(7, tablaHash.darTamanio());
		
		//darValor elementos no existen
		assertEquals(null, tablaHash.darValor("a"));
		assertEquals(null, tablaHash.darValor("b"));
		assertEquals(null, tablaHash.darValor("c"));


		//Pruebas sobre escenario16Elementos

		inicializarTablaHash(16/4);
		escenario16Elementos();
		

		//Comprobar factor de carga
		assertTrue(tablaHash.darFactorDeCarga() <= 8 && tablaHash.darFactorDeCarga() >= 2);

		//darValor 9 elementos iniciales
		assertEquals(new Integer(11), tablaHash.darValor("E"));
		assertEquals(new Integer(16), tablaHash.darValor("S"));
		assertEquals(new Integer(14), tablaHash.darValor("T"));
		assertEquals(new Integer(8), tablaHash.darValor("R"));
		assertEquals(new Integer(7), tablaHash.darValor("U"));
		assertEquals(new Integer(5), tablaHash.darValor("C"));
		assertEquals(new Integer(13), tablaHash.darValor("A"));
		assertEquals(new Integer(12), tablaHash.darValor("D"));
		assertEquals(new Integer(15), tablaHash.darValor("O"));
		assertEquals(9, tablaHash.darTamanio());

		//darValor elementos no existen
		assertEquals(null, tablaHash.darValor("a"));
		assertEquals(null, tablaHash.darValor("b"));
		assertEquals(null, tablaHash.darValor("c"));


		//Pruebas sobre escenarioTablaVacia

		vaciarTablaHash();

		//Esta vez no probamos el factor de carga, porque 0/k, siempre va a dar 0, para cualquier k != 0
		
		//darValor 10 elementos no existen
		assertEquals(0, tablaHash.darTamanio());
		assertEquals(null, tablaHash.darValor("E") );
		assertEquals(null, tablaHash.darValor("S") );
		assertEquals(null, tablaHash.darValor("T") );
		assertEquals(null, tablaHash.darValor("R") );
		assertEquals(null, tablaHash.darValor("U") );
		assertEquals(null, tablaHash.darValor("C") );
		assertEquals(null, tablaHash.darValor("A") );

		//agregar 5 elementos 		
		tablaHash.insertar("D", 0);
		tablaHash.insertar("A", 5);
		tablaHash.insertar("T", 10);
		tablaHash.insertar("O", 15);
		tablaHash.insertar("S", 20);

		//darValor 5 elementos agregados
		assertEquals(new Integer(0), tablaHash.darValor("D") );
		assertEquals(new Integer(5), tablaHash.darValor("A") );
		assertEquals(new Integer(10), tablaHash.darValor("T") );
		assertEquals(new Integer(15), tablaHash.darValor("O") );
		assertEquals(new Integer(20), tablaHash.darValor("S") );
		

		//Comprobar factor de carga
		assertTrue(tablaHash.darFactorDeCarga() <= 8 && tablaHash.darFactorDeCarga() >= 2);

		//Pruebas sobre escenarioAleatorio10000Elementos

		inicializarTablaHash(10000/4);

		escenarioAleatorio10000Elementos();
		

		//Comprobar factor de carga
		assertTrue(tablaHash.darFactorDeCarga() <= 8 && tablaHash.darFactorDeCarga() >= 2);
		
		
		for (int casos = 0; casos < 3; casos++)
		{
			if (casos == 0)
				testPunto9Escenario1K();
			else if (casos == 1)
				testPunto9Escenario5K();
			else
				testPunto9Escenario10K();
				
			
			for (int i = 0; i < arreglosDeHistogramas[casos].length; i++)
			{
				long inicioDraw = System.currentTimeMillis();
				System.out.println("-----------------------------\nDatos histograma actual: K = " + ((i+1)*10) );
				arreglosDeHistogramas[casos][i].draw();
				//Estoy dibujando el histograma actual, y espero 8 segundos a que carge y luego pasa al siguiente			
				while (System.currentTimeMillis() - inicioDraw < TIEMPO_DE_VISUALIZACION){}

				StdDraw.clear();
			}
		}	
		System.out.println("\fFinal");

	}
	//Matriz de 2 dimensiones que me guarda los histogramas generdos por las pruebas con diferentes K por cada diferente N
	//Convenci�n: arreglosDeHistrogramas[a][b]; donde:
	//			a es el escenario a probar (0, el de N = 1K ; 1, el de N = 5K ; 2, el de N = 10K)
	//			b es el escenario de K respectivo. (0, el de K = 10 ; 1, el de K = 20; ... ; 9, el de K = 100) 
	private Histogram[][] arreglosDeHistogramas = new Histogram[3][10];

	private void testPunto9Escenario1K()
	{
		int NEscenario = 1000;
		//Como en este test, los valores no tienen mucha importancia, basta con calcularlos una vez
		int[] valores = generador.generarNumeros(NEscenario);
		
		System.out.println("___________________________\nCaso 1: 1000 elementos (1K)");
		
		for (int k = 1; k <= INTERVALO_K; k++)
		{
			Histogram actual = new Histogram(NEscenario/5 +1);
			
			long tiempoEjecucion = System.currentTimeMillis();

			for( int muestreos = 0; muestreos < MUESTREOS; muestreos++)
			{
				inicializarTablaHash(NEscenario/4);
				String[] llaves = generador.generarCadenas(NEscenario, k*10);

				for (int i = 0; i < NEscenario; i++)
					tablaHash.insertar(llaves[i], valores[i]);

				for (Integer tamanioColisiones : tablaHash.darLongitudListas())
					actual.addDataPoint(tamanioColisiones);
			}
			arreglosDeHistogramas[0][k-1] = actual;
			tiempoEjecucion = System.currentTimeMillis() - tiempoEjecucion;
			System.out.println("Tiempo ejecuci�n promedio (K = " + k + ")=: " + (double) ((double)tiempoEjecucion/MUESTREOS));
		}
		System.out.println("___________________________");
	}

	private void testPunto9Escenario5K()
	{
		int NEscenario = 5000;
		//Como en este test, los valores no tienen mucha importancia, basta con calcularlos una vez
		int[] valores = generador.generarNumeros(NEscenario);
		
		System.out.println("___________________________\nCaso 2: 5000 elementos (5K)");
		
		for (int k = 1; k <= INTERVALO_K; k++)
		{
			Histogram actual = new Histogram(NEscenario/5 +1);
			long tiempoEjecucion = System.currentTimeMillis();

			for( int muestreos = 0; muestreos < MUESTREOS; muestreos++)
			{
				inicializarTablaHash(NEscenario/4);
				String[] llaves = generador.generarCadenas(NEscenario, k*10);

				for (int i = 0; i < NEscenario; i++)
					tablaHash.insertar(llaves[i], valores[i]);

				for (Integer tamanioColisiones : tablaHash.darLongitudListas())
					actual.addDataPoint(tamanioColisiones);
				//				System.out.println("Tama�o tabla: " + tablaHash.darTamanio() + " Bucket size: " + tablaHash.darLongitudListas().length);
			}
			arreglosDeHistogramas[1][k-1] = actual;

			tiempoEjecucion = System.currentTimeMillis() - tiempoEjecucion;
			System.out.println("Tiempo ejecuci�n promedio (K = " + k + ")=: " + (double) ((double)tiempoEjecucion/MUESTREOS));
		}
		System.out.println("___________________________");
	}

	private void testPunto9Escenario10K()
	{
		int NEscenario = 10000;
		//Como en este test, los valores no tienen mucha importancia, basta con calcularlos una vez
		int[] valores = generador.generarNumeros(NEscenario);
		
		System.out.println("___________________________\nCaso 3: 10000 elementos (10K)");
		
		for (int k = 1; k <= INTERVALO_K; k++)
		{			
			Histogram actual = new Histogram(NEscenario/5 +1);
			long tiempoEjecucion = System.currentTimeMillis();

			for( int muestreos = 0; muestreos < MUESTREOS; muestreos++)
			{
				inicializarTablaHash(NEscenario/4);
				String[] llaves = generador.generarCadenas(NEscenario, k*10);

				for (int i = 0; i < NEscenario; i++)
					tablaHash.insertar(llaves[i], valores[i]);

				for (Integer tamanioColisiones : tablaHash.darLongitudListas())
					actual.addDataPoint(tamanioColisiones);
				//				System.out.println("Tama�o tabla: " + tablaHash.darTamanio() + " Bucket size: " + tablaHash.darLongitudListas().length);
			}
			arreglosDeHistogramas[2][k-1] = actual;
			tiempoEjecucion = System.currentTimeMillis() - tiempoEjecucion;
			System.out.println("Tiempo ejecuci�n promedio (K = " + k + ")=: " + (double) ((double)tiempoEjecucion/MUESTREOS));
		}
		System.out.println("___________________________");
	}

}
