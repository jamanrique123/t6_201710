package VO;

import com.google.gson.annotations.SerializedName;

public class VOInfoPelicula 
{
	@SerializedName("Title")
	private String titulo;
	
	@SerializedName("Year")
	private int anio;

	@SerializedName("Director")
	private String director;

	@SerializedName("Actors")
	private String actores;

	@SerializedName("imdbRating")
	private double ratingIMBD;

	public void setTitulo(String titulo)
	{
		this.titulo = titulo;
	}

	public void setAnio(int anio)
	{
		this.anio = anio;
	}

	public void setDirector(String director)
	{
		this.director = director;
	}

	public void setActores(String actores)
	{
		this.actores = actores;
	}

	public void setRatingIMBD(double ratingIMBD)
	{
		this.ratingIMBD = ratingIMBD;
	}

	public String getTitulo()
	{
		return titulo;
	}

	public int getAnio()
	{
		return anio;
	}

	public String getDirector()
	{
		return director;
	}

	public String getActores()
	{
		return actores;
	}

	
	public double getRatingIMBD() 
	{
		return ratingIMBD;
	}
	
	
}