package VO;

import com.google.gson.annotations.SerializedName;

public class VODataPelicula 
{
	@SerializedName("movieId")
	private String id;
	
	@SerializedName("imdbData")
	private VOInfoPelicula pelicula;
	
	public VOInfoPelicula getPelicula() {
		return pelicula;
	}

	public void setPelicula(VOInfoPelicula pelicula) {
		this.pelicula = pelicula;
	}

	public String getId() 
	{
		return id;
	}

	public void setId(String id) 
	{
		this.id = id;
	}
}
