package api;

import java.util.Iterator;

public interface IListaLlaveValorSecuencial <K, V> {
	
	public int darTamanio();
	
	public boolean estaVacia();
	
	public boolean tieneLlave(K llave);
	
	public V darValor(K llave);
	
	public void insertar(K llave, V valor);
	
	public Iterator<K> llaves();	

}
